format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

run:
	cargo run

install:
	cargo clean &&\
		cargo build -j 1

build:
	docker build -t rust_actix_drink_vending_machine .

dockerrun:
	#docker run -it --rm -p 8080:8080 rust_actix_drink_vending_machine
	docker run -dp 8080:8080 rust_actix_drink_vending_machine

#### Cargo Lambda Section ####
## Watches for changes and rebuilds
# watch:
# 	# cargo lambda watch
# 	cargo lambda watch -p 8080 -a 127.0.0.1

# invoke:
# 	cargo lambda invoke \
# 		--data-ascii '{"name": "Oliver"}' \
# 		--output-format json

### Build for AWS Lambda (use arm64 for AWS Graviton2)
# build:
# 	cargo lambda build --release --arm64

# deploy:
# 	cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::xxxxxx:role/ids721

### Invoke on AWS
# aws-invoke:
# 	cargo lambda invoke --remote week2-mini-project --data-ascii '{"name": "aaabbbbbcccccccc"}' \
# 		--output-format json


all: format lint test run