/* Drink Vending Machine Logic */
use serde::Serialize;

#[derive(Serialize)]
pub struct Drink {
    pub name: String,
    pub price: u32,
}

pub fn get_drink_list() -> Vec<Drink> {
    vec![
        Drink {
            name: "Coke".to_string(),
            price: 150,
        },
        Drink {
            name: "Pepsi".to_string(),
            price: 150,
        },
        Drink {
            name: "Water".to_string(),
            price: 100,
        },
    ]
}

pub fn purchase_drink(drink_name: &str, amount: u32) -> Result<String, String> {
    let drinks = get_drink_list();
    if let Some(drink) = drinks.iter().find(|&d| d.name == drink_name) {
        if amount >= drink.price {
            Ok(format!("Purchased {} for ${}", drink.name, drink.price))
        } else {
            Err(format!("Insufficient amount. {} costs ${}", drink.name, drink.price))
        }
    } else {
        Err("Drink not found".to_string())
    }
}

// Test logic here
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_purchase_drink() {
        assert_eq!(
            purchase_drink("Coke", 200).unwrap(),
            "Purchased Coke for $150"
        );
        assert_eq!(
            purchase_drink("Pepsi", 150).unwrap(),
            "Purchased Pepsi for $150"
        );
        assert!(purchase_drink("Unknown", 150).is_err());
        assert!(purchase_drink("Coke", 100).is_err());
    }
}
