use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rust_actix_drink_vending_machine::{get_drink_list, purchase_drink};
use serde::Deserialize;

// Root route for Drink Vending Machine
async fn root() -> impl Responder {
    HttpResponse::Ok().body("Welcome to the Drink Vending Machine")
}

#[derive(Deserialize)]
struct PurchaseInfo {
    drink: String,
    amount: u32,
}

// Purchase a drink
async fn purchase(info: web::Query<PurchaseInfo>) -> impl Responder {
    match purchase_drink(&info.drink, info.amount) {
        Ok(message) => HttpResponse::Ok().body(message),
        Err(error) => HttpResponse::BadRequest().body(error),
    }
}

// Get drink list
async fn drinks() -> impl Responder {
    let drinks = get_drink_list();
    HttpResponse::Ok().json(drinks)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(root))
            .route("/purchase", web::get().to(purchase))
            .route("/drinks", web::get().to(drinks))
    })
    // Listen on the url 127.0.0.1 will cause the application only accessible from inside the container
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
